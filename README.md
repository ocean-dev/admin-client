# Admin Client

The Admin Client is used to administrate the system. Written in
[OceanFront][https://github.com/OceanDev/oceanfront], it allows administrators
to manage all resources and all data in the system. It runs on stationary
computers, on laptops and on mobile devices and features easy management via
drag-and-drop and a modern UX design.

The Admin Client can be extended by users to fit the administration needs of any
system, or serve as a template for other, more specialised administration tools.


## Installation

First start the common infrastructure, if it isn't already running:
```
  docker-compose -f ../ocean/common.yml up --build
```
Then, to run only the Admin client:
```
  docker-compose -f ../ocean/ocean.yml up --build admin_client
```

## Running the RSpecs

If you intend to run RSpec tests, start the common infrastructure, if it isn't
already running:
```
  docker-compose -f ../ocean/common.yml up --build
```
Then, to run the actual specs:
```
  docker-compose -f ../ocean/ocean.yml run --rm -e LOG_LEVEL=fatal admin_client rspec
```
